import logging
import sys
Logger = None
def _reset_logger(log, tag=''):
    for handler in log.handlers:
        handler.close()
        log.removeHandler(handler)
        del handler
    log.handlers.clear()
    log.propagate = False
    console_handle = logging.StreamHandler(sys.stdout)
    console_handle.setFormatter(
        logging.Formatter(
            "[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d] - %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
    )
    file_handle = logging.FileHandler("run-"+tag+".log", encoding="utf-8")
    file_handle.setFormatter(
        logging.Formatter(
            "[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d] - %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
    )
    log.addHandler(file_handle)
    log.addHandler(console_handle)


def _get_logger(tag, log_level = logging.INFO):
    log = logging.getLogger("log")
    _reset_logger(log,tag)
    log.setLevel(log_level)
    return log

def init(tag, log_level = logging.INFO):
    global Logger
    Logger = _get_logger(tag, log_level)
    
Logger = _get_logger('mj-web', logging.INFO)