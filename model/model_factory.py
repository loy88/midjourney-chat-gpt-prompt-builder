MODEL_CACHE={}
from model.model import * 
from model.chatgpt import ChatGPTModel
def create_model(model_type):
    if model_type==MODEL_CHATGPT:
        model = MODEL_CACHE.get(model_type, ChatGPTModel())
        MODEL_CACHE[model_type] = model
        return model
    return None
 